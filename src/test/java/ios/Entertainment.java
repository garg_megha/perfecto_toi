package ios;

import static com.qmetry.qaf.automation.step.CommonStep.click;

import java.util.List;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;

public class Entertainment extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "btn.hindi")
	private QAFWebElement hindi;

	public QAFWebElement getHindi() {
		return hindi;
	}
	@FindBy(locator = "list.news.hindiPAge")
	private List<QAFWebElement> productlist;

	public List<QAFWebElement> getProductlist() {
		return productlist;
	}
	@Override
	protected void openPage(PageLocator locator, Object... args) {
	}

	@QAFTestStep(description = "user print the list of movie reviews")
	public void userPrintTheListOfMovieReviews() {
		waitForPageToLoad();
		for (int i = 0; i < getProductlist().size(); i++) {
			System.out.println(getProductlist().size());
			Reporter.log("Movie name :   " + getProductlist().get(i).getText());
		}

	}
	@QAFTestStep(description = "user clicks on hindi")
	public void userClicksOnHindiLocator() {
		waitForPageToLoad();
		click("btn.hindi");
	}

	@QAFTestStep(description = "user clicks on bollywood")
	public void userClicksOnBollywood() {
		waitForPageToLoad();
		click("btn.bollywood");
	}

}
