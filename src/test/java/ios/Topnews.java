package ios;

import static com.qmetry.qaf.automation.step.CommonStep.click;

import org.hamcrest.Matchers;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.MessageTypes;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class Topnews extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "btn.headlines.toppage")
	private QAFWebElement headLine;

	public QAFWebElement getHeadLine() {
		return headLine;
	}

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
	}

	@QAFTestStep(description = "user clicks on news headline")
	public void userClicksOnNewsHeadline() {
		waitForPageToLoad();
		getHeadLine().click();
		String headLines = getHeadLine().getText();
	}
	@QAFTestStep(description = "user clicks on back button of news")
	public void userClicksOnBackButtonOfNews() {

	}

	@QAFTestStep(description = "user clicks on save item button")
	public void userClicksOnSaveItemButton() {

		String a = String.format(
				ConfigurationManager.getBundle().getString("contentdesc.all", "Save"));
		System.out.println(new QAFExtendedWebElement(a).isDisplayed());

	}

	@QAFTestStep(description = "user verify {0} button is disabled")
	public void userVerifyButtonIsDisabled(String str0) {
		if (CommonStep.verifyDisabled("btn.fontsize.small.newsPage")) {
			Reporter.log("Font size succefully verified " + MessageTypes.Pass);
		}

	}
	@QAFTestStep(description = "user opens top news and capture the news headlines and verify")
	public void userOpensTopNews() {
		waitForPageToLoad();
		String headLines = new QAFExtendedWebElement("US.list.headlines").getText();
		Reporter.log("Top most Headlines found is:" + headLines);
		CommonStep.click("US.list.headlines");
		waitForPageToLoad();
		String headLinesdetails =
				new QAFExtendedWebElement("US.list.secondheadline").getText();
		Validator.verifyThat("News opened and found is Headline", headLines,
				Matchers.containsString(headLinesdetails));

	}

	@QAFTestStep(description = "user clicks on topnews")
	public void userClicksOnTopnews() {
		waitForPageToLoad();
		click("btn.options.toppage");
	}

}
