package ios;

import org.hamcrest.Matchers;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class World {

	@FindBy(locator = "btn.topnews.worldpage")
	private QAFWebElement newsheadLine;

	public QAFWebElement getnewsHeadLine() {
		return newsheadLine;
	}

	@FindBy(locator = "btn.heading.newsdetails.worldpage")
	private QAFWebElement newsDetailsHeadline;

	public QAFWebElement getnewsDetailsHeadLine() {
		return newsDetailsHeadline;
	}

	String news = newsheadLine.getText();
	String news1 = newsDetailsHeadline.getText();

	@QAFTestStep(description = "user opens top news")
	public void userOpensTopNews() {

		Reporter.log("News headline is:" + news);
		getnewsHeadLine().click();

	}

	@QAFTestStep(description = "user verify the news")
	public void userVerifyTheNews() {

		Reporter.log("Details headline is: " + news1);

		Validator.verifyThat(news1, Matchers.containsString(news));

	}

}
