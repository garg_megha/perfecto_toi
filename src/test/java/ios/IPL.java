/**
 * 
 */
package ios;

import org.hamcrest.Matchers;

import com.qmetry.qaf.automation.step.NotYetImplementedException;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

/**
 * @author Megha.Garg
 */
public class IPL {
	@FindBy(locator = "btn.headline.iplnews")
	private QAFWebElement iplheadline;

	public QAFWebElement getiplheadline() {
		return iplheadline;
	}
	@FindBy(locator = "btn.savedStories")
	private QAFWebElement savedStories;

	public QAFWebElement getsavedStories() {
		return savedStories;
	}

	@QAFTestStep(description = "user verify saved stories")
	public void userVerifySavedStories() {
		String iplheadline = getiplheadline().getText();
		String savedstories = getsavedStories().getText();
		Reporter.log("IPL headline: " + iplheadline);
		Reporter.log("Saved stories headline is: " + savedstories);
		Validator.verifyThat(iplheadline, Matchers.containsString(savedstories));
	}

}
