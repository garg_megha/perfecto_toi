package ios;

import org.hamcrest.Matchers;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class ProfilePage {
	@FindBy(locator = "btn.city.profilePage")
	private QAFWebElement cityName;

	public QAFWebElement getcityName() {
		return cityName;
		
	}


	@QAFTestStep(description = "user verify the city is {0}")
	public void userVerifyTheCity(String cityname) {

		String citySelected= new QAFExtendedWebElement("btn.city.profilePage").getText();
		
	if(Validator.verifyThat(cityname, Matchers.containsString(new QAFExtendedWebElement("btn.city.profilePage").getText()))) 
	{
		Reporter.logWithScreenShot("City name correctly selected and verified."+ citySelected );
	}
	}

}
