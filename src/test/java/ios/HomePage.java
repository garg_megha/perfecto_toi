/**
 * 
 */
package ios;

import static com.qmetry.qaf.automation.step.CommonStep.click;

import com.perfecto_toi_group3.utils.PerfectoUtilities;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;

/**
 * @author Megha.Garg
 */
public class HomePage extends WebDriverBaseTestPage<WebDriverTestPage> {
	@Override
	protected void openPage(PageLocator locator, Object... args) {

	}
	public QAFExtendedWebElement creatElement(String loc, String key) {

		return new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString(loc), key));
	}
	@QAFTestStep(description = "user opens the application")
	public void userOpensTheApplication() {
		PerfectoUtilities.openApplication("TOI");
	}

	@QAFTestStep(description = "user clicks on {0}")
	public void userClicksOn(String btn) {
		// CommonStep.waitForPresent("contentdesc.all");
		click(String.format(ConfigurationManager.getBundle().getString("contentdesc.all"),
				btn));
	}

	@QAFTestStep(description = "user clicks on {0} button")
	public void userButton(String btn) {
		click(String.format(ConfigurationManager.getBundle().getString("text.all"), btn));
	}
	@QAFTestStep(description = "user clicks on {0} button and send character {1}")
	public void userClicksOnButtonAndSendCharacter(String btn, String text) {
		QAFExtendedWebElement element = new QAFExtendedWebElement(btn);
		element.sendKeys(text);
		// element.sendKeys("radiobtn.city.profilepage");
		click("radiobtn.city.profilepage");
		// element.sendKeys(Keys.ENTER);
	}

	@QAFTestStep(description = "user clicks on {0} locator")
	public void userClicksOnLocator(String loc) {
		waitForPageToLoad();
		new QAFExtendedWebElement(loc).click();

	}

	@QAFTestStep(description = "user clicks on search button")
	public void userClicksOnSearchButton() {
		waitForPageToLoad();
		click("btn.search.homepage");
	}

	@QAFTestStep(description = "user clicks on chage profilepage")
	public void userClicksOnChageProfilepage() {
		waitForPageToLoad();
		click("btn.change.profilepage");
	}

}
