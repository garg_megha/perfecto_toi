package com.perfecto_toi_group3.utils;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriverException;

import com.qmetry.qaf.automation.core.MessageTypes;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.util.Reporter;

import io.appium.java_client.AppiumDriver;


/**
 * @author Sonal.Sathe
 */

public class PerfectoUtilities {
	
	
	
	static int timeout = 30;
	private static AppiumDriver getDriver() {
		return DriverUtils.getAppiumDriver();
	}

	public static void openApplication(String paramString) {
		String command = "mobile:application:open";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("name", paramString);
		params.put("timeout", 60);
		//getDriver().executeScript(command, params);
		new WebDriverTestBase().getDriver().executeScript(command, params);
		
	}

	public static void closeApplication(String paramString) {
		try {
			String command = "mobile:application:close";
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("name", paramString);
			getDriver().executeScript(command, params);
		} catch (WebDriverException e) {
			Reporter.log(paramString + " application did not close/already closed", MessageTypes.Info);
		}
	}

}
