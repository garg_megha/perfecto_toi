package com.perfecto_toi_group3.utils;

import org.openqa.selenium.remote.DriverCommand;

import com.qmetry.qaf.automation.ui.webdriver.CommandTracker;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElementCommandListener;


public class WEListener implements QAFWebElementCommandListener {

	@Override
	public void afterCommand(QAFExtendedWebElement arg0, CommandTracker arg1) {
		
		
	}

	@Override
	public void beforeCommand(QAFExtendedWebElement element, CommandTracker commandTracker) {
		if(commandTracker.getCommand().equalsIgnoreCase(DriverCommand.SEND_KEYS_TO_ELEMENT))
			element.clear();
		
		
		
	
	}

	@Override
	public void onFailure(QAFExtendedWebElement arg0, CommandTracker arg1) {
		
	}

}
